window.addEventListener('load', function () { 

    // NodeList.prototype.forEach = function (callback) {
    //     Array.prototype.forEach.call(this, callback);
    // }


    var form  = document.getElementsByTagName('form')[0];
    var sections = document.getElementsByTagName('section');
    var formFields = document.querySelectorAll('input, select');
    var errors = document.getElementsByClassName('error');
    var submitButton = document.getElementById('send');
    var navLeft = document.getElementsByClassName('left');
    var navRight = document.getElementsByClassName('right');

    var delFiles = document.getElementById('delFiles');

    var telephone = document.getElementById('telephone');

    var searchInput = document.getElementById("search");
    var suggestionList = document.getElementsByClassName("suggestionList")[0];    

    var warehouseSelect = document.getElementById('warehouseSelect');
    var warehouses; 
    var cities;

    var cardYear = document.getElementById('cardYear');
    var cardInputs = document.getElementsByClassName('cardNumber');

    formFields.forEach = [].forEach;
    navLeft.forEach = [].forEach;
    navRight.forEach = [].forEach;
    suggestionList.forEach = [].forEach;
    cardInputs.forEach = [].forEach;
    sections.forEach = [].forEach;
    

    function getCities() {
        var expiry = 30*24*60*60;
        var time = localStorage.getItem('novaposhta:cities:time');
        if(time) var diff = (Date.now() - time)/1000;
        if(time && (diff < expiry)) {
            cities = JSON.parse(localStorage.getItem('novaposhta:cities'));            
        }
        else {
            localStorage.removeItem('novaposhta:cities');
            loadCities();
        }
                
    }

    getCities();


    function loadCities() {

        var options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "modelName": "AddressGeneral",
                "calledMethod": "getCities",
                "methodProperties": {
                    "Page": "1"
                },
                "apiKey": "39c1d2de244fadf8e13c161055dcb8c7"
            })
        }


        fetch('http://api.novaposhta.ua/v2.0/json/AddressGeneral/getCities/', options)
            .then((resp) => {
                if(resp.status == 200) return resp;
                throw Error(resp.status);
            })
            .then((resp) => {
                if(resp.headers.get('Content-Type') == 'application/json') return resp.json();
                if(resp.headers.get('Content-Type') == 'text/html') throw Error(resp.text());
            })
            .then((json) => {
                if(!json.success) return console.log(json.errors[0]);
                if(json.data) {
                    localStorage.setItem('novaposhta:cities', JSON.stringify(json.data));
                    localStorage.setItem('novaposhta:cities:time', Date.now());

                    cities = JSON.parse(localStorage.getItem('novaposhta:cities'));
                }
            })
            .catch((error) => console.log(error));
      
      }


      function getWarehouses() {
        var expiry = 24*60*60;
        var time = localStorage.getItem('novaposhta:warehouses:time');
        if(time) var diff = (Date.now() - time)/1000;
        if(time && (diff < expiry)) {
            warehouses = JSON.parse(localStorage.getItem('novaposhta:warehouses'));            
        }
        else {
            localStorage.removeItem('novaposhta:warehouses');
            loadWarehouses();
        }
                
    }

    getWarehouses();


    function loadWarehouses(page) {
        var page = page || '1';
        var options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "modelName": "AddressGeneral",
                "calledMethod": "getWarehouses",
                "methodProperties": {
                    "Language": "ru"
                },
                "apiKey": "39c1d2de244fadf8e13c161055dcb8c7"
            })
        }

        fetch('http://api.novaposhta.ua/v2.0/json/AddressGeneral/getWarehouses/', options)
            .then((resp) => {
                if(resp.status == 200) return resp;
                throw Error(resp.status);
            })
            .then((resp) => {
                if(resp.headers.get('Content-Type') == 'application/json') return resp.json();
                if(resp.headers.get('Content-Type') == 'text/html') throw Error(resp.text());
            })
            .then((json) => {
                if(!json.success) return console.log(json.errors[0]);//throw Error(json.errors);
                if(json.data) {
                    console.log(json.data.length)
                    localStorage.setItem('novaposhta:warehouses', JSON.stringify(json.data));
                    localStorage.setItem('novaposhta:warehouses:time', Date.now());

                    warehouses = JSON.parse(localStorage.getItem('novaposhta:warehouses'));

                }
            })
            .catch((error) => console.log(error));
      
      }

    

    //Submit 

    submitButton.addEventListener('click', (event) => {
        var errors = 0;
        formFields.forEach((el) => {
            if(!el.validity.valid) {
                event.preventDefault();
                errors++  
                checkErrors(el);             
            }
        });

        if(errors > 0) {
            return
        }
        form.submit();
    });

    //Navigation 

    navLeft.forEach(el => {
        el.addEventListener('click', (e) => {
            el.parentNode.style.display = 'none';
            el.parentNode.previousElementSibling.style.display = 'block';
        });
    });

    navRight.forEach(el => {
        el.addEventListener('click', (e) => {               
            el.parentNode.style.display = 'none';
            el.parentNode.nextElementSibling.style.display = 'block';         
        });
    });

    //Errors

    formFields.forEach(el => {
        el.addEventListener('keydown', (event) => {
            if(event.which == 13) event.preventDefault();
        });

        el.addEventListener('input', checkErrors);    
        el.addEventListener('change', checkErrors);    
    });

    

    function checkErrors(arg) {
        var field = arg.target || arg;
        var error = field.parentNode.querySelector('.error') || field.parentNode.parentNode.querySelector('.error');

        if(field.validity.valid) {
            error.innerHTML = '';
            error.className = "error";
        }

        else if(field.validity.valueMissing) {
            error.className = 'error active';
            error.innerHTML = field.name + ' is required';
        }

        else if(field.validity.typeMismatch) {

            if(field.name == 'email') {
                error.className = 'error active';
                error.innerHTML = 'This does not look like email. Please, enter email.';
            }

            
        }

        else if(field.validity.patternMismatch) { 

            if(field.className == 'cardNumber') {
                error.className = 'error active';
                error.innerHTML = 'This is not a card number. Please, enter a 14 digit number.';
            }
            
            if(field.name == 'telephone') {
                error.className = 'error active';
                error.innerHTML = 'Please, enter Ukrainian mobile number.';
            }
        }
    }

    form.addEventListener('input', toggleNav);
    form.addEventListener('change', toggleNav);

    

    function toggleNav() {

        var errors = 0;

        sections.forEach(section => {
            var navRightButton = section.querySelector('.right');
            
            if(!navRightButton) return;                

                if(section.id == 'first-section') {

                    var images = section.querySelectorAll('.image');
                    images.forEach = [].forEach;                    
                    
                    var n = 0;

                    //there should be at least 1 image on a first page
    
                    images.forEach(image => {
                        if(image.files[0]) {
                            n++;
                            return;
                        }
    
                    });
    
                    if(n < 1) errors = 1;
                }
    
            var sectionFields = section.querySelectorAll('input, select');
            sectionFields.forEach = [].forEach;
    
    
            sectionFields.forEach((el) => {
                if(!el.validity.valid) {
                    errors++             
                }
            });
    
            if(!errors) {
                navRightButton.style.visibility = 'visible';
            }
            else {
                navRightButton.style.visibility = 'hidden';
            }
        });

    }

    

    //File

    form['file'].addEventListener('change', (e) => {
        var fileInput = e.target;
        fileInput.parentNode.querySelector('.error').innerHTML = '';
        for(i=0; i < fileInput.files.length - 1; i++) {
            if(fileInput.files[i].size > 1e7) {
                fileInput.parentNode.querySelector('.error').innerHTML += fileInput.files[i].name + 'is too big. No more than 10 megabytes.';
            }
            else if(fileInput.files[i].size < 1e4 ) {
                fileInput.parentNode.querySelector('.error').innerHTML += fileInput.files[i].name + 'is too small. No less than 10 kilobytes.';
            }
        }
        if(fileInput.files.length > 0) {
            delFiles.style.display = 'inline-block';

        }
        
    });

    delFiles.addEventListener('click', () => {
        form['file'].value = null;
        delFiles.style.display = 'none';
        toggleNav(); 
    });

    //Image

    form.addEventListener('click', (e) => {
        if(e.target && e.target.id == "imageSelect") {
            e.target.previousElementSibling.previousElementSibling.click(); 
        }
    });

    form.addEventListener('change', (e) => {
        if(e.target && e.target.className == "image") {
            var files = e.target.files;
            var imageSelect = e.target.nextElementSibling.nextElementSibling;
            var imagePreview = imageSelect.nextElementSibling;
            
            if (!files.length) {
                imagePreview.innerHTML = "<p>No images selected!</p>";
            }
    
            else {
                imagePreview.innerHTML = '';
                    
                    var img = document.createElement("img");
                    img.src = window.URL.createObjectURL(files[0]);
                    img.onload = function() {
                        window.URL.revokeObjectURL(this.src);
                    }
                    imagePreview.appendChild(img);
    
                    var info = document.createElement("span");
                    info.className = 'info';                
                    info.innerHTML = files[0].name + ': ' + files[0].size + ' bytes';
                    imagePreview.appendChild(info);
    
                    var del = document.createElement('span');
                    del.className = 'deleteImage';
                    del.innerHTML = 'x';                
                    imagePreview.appendChild(del);


                    var imageUploadContainer = del.parentNode.parentNode;
                    var mainParent = imageUploadContainer.parentNode;
    
                    del.addEventListener('click', () => {  
                        
                        mainParent.removeChild(imageUploadContainer);
                        toggleNav();                
                        
                    });
    
                    
                    var nextNode = imageUploadContainer.nextElementSibling;                    
                    var newImageUpload = document.createElement("div");
                    newImageUpload.className = 'image-upload-container';
                    var newNode = mainParent.insertBefore(newImageUpload, nextNode);
    
                    imageUploadContainer.removeChild(imageSelect);
    
                    newNode.innerHTML = "<input type='file' name='image' class='image' class='field-section-1' accept='image/*'>" +
                                        "<p class='error'></p>" +
                                        "<div id='imageSelect'>Select image</div>" +
                                        "<div id='imagePreview'></div>";
            }
        }
    });

    //Telephone

    telephone.addEventListener('input', e => {
        var value = e.target.value.trim();

        if(/^\+38\s0\d{2}$/.test(value)) {
            telephone.value = value + ' ';
        }

    });

    telephone.addEventListener('focus', e => {
        var value = e.target.value.trim();
        if(value == '') telephone.value = '+38 0';
    });

    // Autocomplete
    
    searchInput.addEventListener('input', inputHandler);

    suggestionList.addEventListener('click', (e) => {

        suggestionList.querySelectorAll('.option').forEach((el) => {
            el.classList.remove('highlight');
        });

        e.target.classList.add('highlight');

        searchInput.value = e.target.innerHTML;
        populateWarehouseSelect(e.target.id);
        searchInput.focus();
        suggestionList.style.display = 'none'; 
    });

    searchInput.addEventListener('keydown', (e) => {

        var options = document.querySelectorAll('.option');
        var length = options.length;
        var first = options[0];
        var last = options[length-1];
        var current = document.querySelector('.highlight');

        if(e.which == 13) e.preventDefault();
        if(e.which == 13 && current) {
            searchInput.value = current.innerHTML;
            suggestionList.style.display = 'none'; 
        }

        if(e.keyCode == 40 && length > 1 && current != last) {
            current.classList.remove('highlight');
            current = current.nextElementSibling;
            current.classList.add('highlight');
            searchInput.value = current.innerHTML; 
        }

        if(e.keyCode == 38 && length > 1 && current != first) {
            current.classList.remove('highlight');
            current = current.previousElementSibling;
            current.classList.add('highlight');
            searchInput.value = current.innerHTML;
        }

        if((e.keyCode == 38 || e.keyCode == 40) && length == 1) searchInput.value = current.innerHTML;

        if(current && e.keyCode == 13) {
            populateWarehouseSelect(current.id);
        }
    });
    
    function inputHandler() {
      var input = searchInput.value.trim();
      warehouseSelect.innerHTML = '<option disabled selected value> -- choose city before selecting warehouse -- </option>';

    
      if (input.length > 0) {
        var searchResult = search(input);
        suggestionResult = renderList(searchResult);
        suggestionList.style.display = 'block';
    
        if(suggestionResult) {
          suggestionList.innerHTML = suggestionResult;
          suggestionList.querySelector('.option').classList.add('highlight');;
        } 
        else {
          suggestionList.innerHTML = '<div id="notFound">Не найдено</div>';
        } 
    
      }   
      else {
        suggestionList.innerHTML = '';
        suggestionList.style.display = 'none';
      }
    
    }
    
    function search(str) {
        
      str = str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
      var pattern = new RegExp('(?:^' + str + ')|(?:\s|-' + str + ')', 'i');
    
      return cities.filter(function (e) { return pattern.test(e.DescriptionRu) });
    }
    
    function renderList(arr) {
      var html_list = '';
    
      if (arr.length > 0) {
    
        arr.slice(0, 6).map(function (e) { html_list += '<div class="option" id=' + e.Ref + '>' + e.DescriptionRu +  '</div>'});

    
        if(arr.length > 5) html_list += '<div id="citiesNumber">Показано 5 из ' + arr.length + ' найденных городов</br>' + 
                                    'Уточните запрос, чтобы увидеть остальные</div>';
        return html_list;
      }
        return false;
    }

    function populateWarehouseSelect(value) {
        warehouseSelect.innerHTML = '';        
        var list = warehouses.filter(el => {
            if(el.CityRef == value) {
                return true
            }
            return false;
        });

        list.forEach(el => {
            warehouseSelect.innerHTML += '<option value=' + el.Ref + '>' + el.DescriptionRu + '</option>';
        });

        warehouseSelect.selectedIndex = '-1'; //thisa way 'change' event occurs and we can chech validity then and show nav
        //warehouseSelect.selectedIndex = '0';

    }
    
    //Card

    (function expirationYear(yearSelect) {
        yearSelect.innerHTML = '';
        var now = new Date();
        var year = now.getFullYear();
        for(i=0; i < 20; i++) {
            yearSelect.innerHTML += '<option value=' + year + '>' + year + '</option>';
            year++;

        }
    })(cardYear);

    cardInputs.forEach((el, index) => {
        el.addEventListener('input', (e) => {
            var value = e.target.value;
            if(value.length == 4 && index != 3) {
                el.nextElementSibling.focus();
            }

            if(value.length == 4 && index == 3) {
                el.blur();
            }
        });
    });
    

});